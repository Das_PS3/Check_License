﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Windows.Forms;

namespace Check_License
{
    static class Misc_Utils
    {
        public static void DeleteEmptyDirs(string dir)
        {
            if (String.IsNullOrEmpty(dir))
                throw new ArgumentException("Starting directory is a null reference or an empty string", "dir");

            try
            {
                foreach (var d in Directory.GetDirectories(dir))
                {
                    DeleteEmptyDirs(d);
                }

                var entries = Directory.GetFileSystemEntries(dir);

                if (!entries.Any())
                {
                    try
                    {
                        Directory.Delete(dir);
                    }
                    catch (UnauthorizedAccessException) { }
                    catch (DirectoryNotFoundException) { }
                }
            }
            catch (UnauthorizedAccessException) { }
        }
        
        public static void ExtractEmbeddedFile(ref string saveAsName, string extension = ".exe")
        {
            // Get Current Assembly refrence
            Assembly currentAssembly = Assembly.GetExecutingAssembly();
            // Get all imbedded resources
            string[] arrResources = currentAssembly.GetManifestResourceNames();

            foreach (string resourceName in arrResources)
            {
                if (resourceName.EndsWith(extension, StringComparison.OrdinalIgnoreCase))
                { //or other extension desired
                    //Name of the file saved on disk
                    saveAsName = resourceName.Substring(Application.ProductName.Length + 1);
                    FileInfo fileInfoOutputFile = new FileInfo(saveAsName);
                    //CHECK IF FILE EXISTS AND DO SOMETHING DEPENDING ON YOUR NEEDS
                    if (fileInfoOutputFile.Exists)
                    {
                        //overwrite if desired  (depending on your needs)
                        //fileInfoOutputFile.Delete();
                    }
                    //OPEN NEWLY CREATING FILE FOR WRITTING
                    using (FileStream streamToOutputFile = fileInfoOutputFile.OpenWrite())
                    //GET THE STREAM TO THE RESOURCES
                    using (Stream streamToResourceFile = currentAssembly.GetManifestResourceStream(resourceName))
                    {
                        //---------------------------------
                        //SAVE TO DISK OPERATION
                        //---------------------------------
                        const int size = 4096;
                        byte[] bytes = new byte[4096];
                        int numBytes;
                        while ((numBytes = streamToResourceFile.Read(bytes, 0, size)) > 0)
                            streamToOutputFile.Write(bytes, 0, numBytes);
                    }
                }//end_if

            }//end_foreach
        }//end_mRecreateAllExecutableResources

        public static byte[] HexStringToByteArray(string hexString)
        {
            return Enumerable.Range(0, hexString.Length)
                             .Where(x => x % 2 == 0)
                             .Select(x => Convert.ToByte(hexString.Substring(x, 2), 16))
                             .ToArray();
        }

        public static byte[] Initialize(this byte[] array, byte defaultValue)
        {
            for (int i = 0; i < array.Length; i++)
                array[i] = defaultValue;

            return array;
        }

        public static bool IsBinaryFile(this string value, int sampleSize = 10240)
        {
            if (!File.Exists(value))
                throw new ArgumentException("File path is not valid", "filePath");

            var buffer = new char[sampleSize];
            string sampleContent;

            using (var sr = new StreamReader(value))
            {
                int length = sr.Read(buffer, 0, sampleSize);
                sampleContent = new string(buffer, 0, length);
            }

            //Look for 3 consecutive binary zeroes
            if (sampleContent.Contains("\0\0\0"))
                return true;

            return false;
        }

        public static void MoveDirectory(string source, string target)
        {
            var stack = new Stack<Folders>();
            stack.Push(new Folders(source, target));

            while (stack.Count > 0)
            {
                var folders = stack.Pop();
                Directory.CreateDirectory(folders.Target);
                foreach (var file in Directory.GetFiles(folders.Source, "*.*"))
                {
                    string targetFile = Path.Combine(folders.Target, Path.GetFileName(file));
                    if (File.Exists(targetFile)) File.Delete(targetFile);
                    File.Move(file, targetFile);
                }

                foreach (var folder in Directory.GetDirectories(folders.Source))
                {
                    stack.Push(new Folders(folder, Path.Combine(folders.Target, Path.GetFileName(folder))));
                }
            }
            Directory.Delete(source, true);
        }

        public static double PrintProgress(ulong progress, ulong size, double previousPercent, string file)
        {
            file = Path.GetFileNameWithoutExtension(file);
            double percent = Math.Round((progress * 100) / (double)size, 1);

            if (percent > previousPercent)
            {
                previousPercent = percent;
                string bar = new String('=', (int)(percent / 4));

                Console.CursorLeft = 0;
                Console.Write("{0} [{1,-25}] {2:0.0}%", file, bar, percent);
            }

            return previousPercent;
        }

        public static uint RoundUp(uint numToRound, uint multiple)
        {
            if (multiple == 0)
            {
                return numToRound;
            }

            uint remainder = numToRound % multiple;

            if (remainder == 0)
                return numToRound;

            return numToRound + multiple - remainder;
        }

        public static ulong RoundUp(ulong numToRound, uint multiple)
        {
            if (multiple == 0)
            {
                return numToRound;
            }

            ulong remainder = numToRound % multiple;

            if (remainder == 0)
                return numToRound;

            return numToRound + multiple - remainder;
        }

        public static string StringWrapper(string str, int columnWidth)
        {
            string[] words = str.Split(' ');

            StringBuilder newSentence = new StringBuilder();

            string line = "";
            foreach (string word in words)
            {
                if ((line + word).Length > columnWidth)
                {
                    newSentence.AppendLine(line);
                    line = "";
                }

                line += String.Format("{0} ", word);
            }

            if (line.Length > 0)
                newSentence.AppendLine(line);

            return newSentence.ToString();
        }

        public static string Truncate(this string value, int maxChars)
        {
            return value.Length <= maxChars ? value : value.Substring(0, maxChars) + "...";
        }
    }

    sealed class Folders
    {
        public string Source { get; private set; }
        public string Target { get; private set; }

        public Folders(string source, string target)
        {
            Source = source;
            Target = target;
        }
    }
}
