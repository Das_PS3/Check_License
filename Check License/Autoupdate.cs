﻿using System;
using System.Diagnostics;
using System.Drawing;
using System.Globalization;
using System.IO;
using System.Net;
using System.Reflection;
using System.Windows.Forms;
using System.Xml;

namespace Check_License
{
    public partial class Autoupdate : Form
    {
        private const string DOWNLOAD_LINK = "http://bloodbane.noip.me/download.php?f=";
        private const string PROGRAMS_XML = "http://bloodbane.noip.me/dasanko/programs.xml";

        private bool updateDownloaded = false;

        public Autoupdate(bool requested = false)
        {
            InitializeComponent();

            Text = Application.ProductName;

            if (File.Exists("no autoupdate"))
                disableBtn.Text = "Enable autoupdate";
            
            AutoupdateCheck(requested);
        }

        private void AutoupdateCheck(bool requested)
        {
            WebClient updater = new WebClient();

            updater.Headers["Accept"] = "application/xml";
            updater.Headers["User-Agent"] = Application.ProductName;

            try
            {
                Assembly thisAssembly = Assembly.GetExecutingAssembly();
                Debug.WriteLine(thisAssembly.FullName);
                Debug.WriteLine(thisAssembly.GetName().Name);

                float currentVersion = float.Parse(Application.ProductVersion, CultureInfo.InvariantCulture);
                Debug.WriteLine(thisAssembly.GetName().Version);
                Debug.WriteLine(currentVersion);
                float onlineVersion = 0;
                string changelog = String.Empty, description = String.Empty;

                using (Stream stream = updater.OpenRead(PROGRAMS_XML))
                using (XmlReader reader = XmlReader.Create(stream))
                {
                    bool stopReading = false;

                    while (reader.Read())
                    {
                        switch (reader.NodeType)
                        {
                            case XmlNodeType.Text:
                                if (reader.Value == Application.ProductName) // If <Name> is this application's name...
                                {
                                    for (int i = 0; i < 4; i++)
                                        reader.Read();
                                    onlineVersion = float.Parse(reader.Value, CultureInfo.InvariantCulture);

                                    for (int i = 0; i < 4; i++)
                                        reader.Read();
                                    changelog = reader.Value;

                                    for (int i = 0; i < 4; i++)
                                        reader.Read();
                                    description = reader.Value;

                                    stopReading = true;
                                }
                                break;
                        }

                        if (stopReading)
                            break;
                    }
                }

                if ((currentVersion >= onlineVersion) && requested)
                {
                    yesBtn.Enabled = false;
                    yesBtn.Hide();

                    noBtn.Text = "Close";
                    noBtn.Location = new Point(12, 242);

                    Color originalColor = richTextBox1.SelectionColor;
                    Font originalFont = richTextBox1.SelectionFont;

                    richTextBox1.AppendText("No new version detected.");
                    richTextBox1.AppendText(String.Format("\n\nChanges in this version: {0}", changelog));

                    ShowDialog();
                }

                else if (currentVersion < onlineVersion)
                {
                    Color originalColor = richTextBox1.SelectionColor;
                    Font originalFont = richTextBox1.SelectionFont;

                    richTextBox1.AppendText("A new version has been detected.\n\nCurrent Version: ");

                    richTextBox1.SelectionColor = Color.Red;
                    richTextBox1.SelectionFont = new Font(originalFont.FontFamily, originalFont.Size - 0.5f, FontStyle.Bold);
                    richTextBox1.AppendText(currentVersion.ToString().Replace(',', '.'));

                    richTextBox1.SelectionColor = originalColor;
                    richTextBox1.SelectionFont = new Font(originalFont.FontFamily, originalFont.Size, FontStyle.Regular);
                    richTextBox1.AppendText("\nNew version: ");

                    richTextBox1.SelectionColor = Color.Green;
                    richTextBox1.SelectionFont = new Font(originalFont.FontFamily, originalFont.Size - 0.5f, FontStyle.Bold);
                    richTextBox1.AppendText(onlineVersion.ToString().Replace(',', '.'));

                    richTextBox1.SelectionColor = originalColor;
                    richTextBox1.SelectionFont = new Font(originalFont.FontFamily, originalFont.Size, FontStyle.Regular);
                    richTextBox1.AppendText(String.Format("\n\nChanges in this version: {0}", changelog));

                    ShowDialog();
                }
            }
            catch (WebException) { Debug.WriteLine("Connection error."); }
        }

        private void disableBtn_Click(object sender, EventArgs e)
        {
            if (disableBtn.Text == "Disable autoupdate")
            {
                using (File.Create("no autoupdate"))
                disableBtn.Text = "Enable autoupdate";
            }

            else if (disableBtn.Text == "Enable autoupdate")
            {
                File.Delete("no autoupdate");
                disableBtn.Text = "Disable autoupdate";
            }
        }

        private void noBtn_Click(object sender, EventArgs e)
        {
            Dispose();
        }

        private void yesBtn_Click(object sender, EventArgs e)
        {
            string programUpdateFile = Application.ProductName + ".rar";

            if (updateDownloaded)
            {
                Process.Start("explorer.exe", String.Format("/select,\"{0}\"", AppDomain.CurrentDomain.BaseDirectory + programUpdateFile));
                Application.Exit();
            }

            WebClient updater = new WebClient();
            try
            {
                updater.DownloadFile(DOWNLOAD_LINK + programUpdateFile, programUpdateFile);

                updateDownloaded = true;

                richTextBox1.Text = "The update has successfully downloaded.\n\nWould you like to close the application now?";
            }

            catch (WebException) { Debug.WriteLine("Connection error."); }
        }

        private void richTextBox1_Enter(object sender, EventArgs e)
        {
            if (yesBtn.Enabled)
                yesBtn.Focus();
            else
                noBtn.Focus();
        }
    }
}
