﻿using System;
using System.Diagnostics;
using System.IO;
using System.Reflection;
using System.Text;
using System.Windows.Forms;

namespace Check_License
{
    class EDAT_Info
    {
        bool PkgExtracted = false;
        EDAT_Information[] edatsInfo;
        string extractedFolder;

        enum type
        {
            edat,
            folder,
            pkg,
        };

        public struct EDAT_Information
        {
            public string filename;
            public string cid;
            public string digest;
            public string version;
            public string compression;
            public string license;
        }

        public EDAT_Information[] GetInformation(string fileOrFolder, bool recursion)
        {
            string[] edats;
            type value;

            if (fileOrFolder.EndsWith(".pkg", StringComparison.OrdinalIgnoreCase))
            {
                edats = ExtractPKG(fileOrFolder);
                value = type.pkg;
            }
            else if (fileOrFolder.EndsWith(".edat", StringComparison.OrdinalIgnoreCase))
            {
                edats = new string[] { fileOrFolder };
                value = type.edat;
            }
            else
            {
                if (recursion == true)
                    edats = Directory.GetFiles(fileOrFolder, "*.edat", SearchOption.AllDirectories);
                else
                    edats = Directory.GetFiles(fileOrFolder, "*.edat", SearchOption.TopDirectoryOnly);
                value = type.folder;
            }

            edatsInfo = new EDAT_Information[edats.Length];

            for (int i = 0; i < edats.Length; i++)
            {
                InfoExtractor(edats[i], ref edatsInfo[i], fileOrFolder, value);
            }

            if (PkgExtracted == true)
            {
                Directory.Delete(extractedFolder, true);
                PkgExtracted = false;
            }

            return edatsInfo;
        }


        string[] ExtractPKG(string file)
        {
            string embeddedFilename = String.Empty;
            Misc_Utils.ExtractEmbeddedFile(ref embeddedFilename);

            var process = new Process { StartInfo = new ProcessStartInfo { FileName = embeddedFilename, Arguments = String.Format("-ex {0}", file) } };
            process.Start();
            process.WaitForExit();

            File.Delete(embeddedFilename);

            extractedFolder = file.Substring(0, file.Length - 4);

            string[] edats = Directory.GetFiles(extractedFolder, "*.edat", SearchOption.AllDirectories);

            PkgExtracted = true;

            return edats;
        }

        void InfoExtractor(string edat, ref EDAT_Information edatInfo, string fileOrFolder, type value)
        {
            switch (value)
            {
                case type.edat:
                    edatInfo.filename = Path.GetFileName(edat);
                    break;
                case type.folder:
                    edatInfo.filename = Path.GetFileName(fileOrFolder) + @"\" + edat.Substring(fileOrFolder.Length + 1);
                    break;
                case type.pkg:
                    edatInfo.filename = edat.Substring(fileOrFolder.Length - 3);
                    break;
            }

            using (FileStream stream = new FileStream(edat, FileMode.Open, FileAccess.Read))
            using (BinaryReader reader = new BinaryReader(stream))
            {
                byte output;

                stream.Seek(0x7, 0);
                edatInfo.version = reader.ReadByte().ToString();

                stream.Seek(0xB, 0);
                output = reader.ReadByte();
                if (output == 3)
                    edatInfo.license = "Free";
                else if (output == 2)
                    edatInfo.license = "Local";
                else if (output == 1)
                    edatInfo.license = "Network (Online)";
                else
                    edatInfo.license = String.Format("Unknown license (returned 0x{0})", output);

                stream.Seek(0x10, 0);
                edatInfo.cid = String.Join("", reader.ReadChars(0x24));

                stream.Seek(0x40, 0);
                byte[] digestArray = reader.ReadBytes(0x10);
                try
                {
                    UTF8Encoding utf8 = new UTF8Encoding(false, true);
                    edatInfo.digest = utf8.GetString(digestArray);
                }
                catch (DecoderFallbackException) { edatInfo.digest = "Original"; }

                stream.Seek(0x83, 0);
                byte compressionB = reader.ReadByte();
                switch (compressionB)
                {
                    case 00:
                    case 12:
                    case 60:
                        edatInfo.compression = "No compression";
                        break;

                    case 01:
                    case 13:
                        edatInfo.compression = "Compressed";
                        break;

                    default:
                        edatInfo.compression = String.Format("Unknown compression status (returned 0x{0})", compressionB);
                        break;
                }
            }
        }
    }
}
