﻿using System;
using System.Drawing;
using System.IO;
using System.Net;
using System.Windows.Forms;

namespace Check_License
{
    public partial class Main_Window : About_System_Menu
    {
        EDAT_Info edatInfo;

        public Main_Window()
        {
            if (!File.Exists("no autoupdate"))
            {
                Autoupdate autoupdate = new Autoupdate();
                autoupdate.Owner = this;
            }

            InitializeComponent();
            edatInfo = new EDAT_Info();
        }

        private void clearBtn_Click(object sender, EventArgs e)
        {
            textBox.Clear();
        }

        private void openFolderBtn_Click(object sender, EventArgs e)
        {
            FolderSelectDialog folderPath = new FolderSelectDialog();
            folderPath.Title = "Select the folder to check";

            if (folderPath.ShowDialog())
                pathTextBox.Text = folderPath.FolderName;
            else
                pathTextBox.Clear();

            fillTextBox();
        }

        private void openFileBtn_Click(object sender, EventArgs e)
        {
            OpenFileDialog filePath = new OpenFileDialog();
            filePath.Title = "Open file to check";
            filePath.Filter = "EDAT, PKG files|*.edat; *.pkg|EDAT files|*.edat|PKG files|*.pkg";

            if (filePath.ShowDialog() == DialogResult.OK)
                pathTextBox.Text = filePath.FileName;
            else
                pathTextBox.Clear();

            fillTextBox();
        }

        private void fillTextBox()
        {
            if (!String.IsNullOrWhiteSpace(pathTextBox.Text))
            {
                statusLabel.ForeColor = Color.Red;
                statusLabel.Text = "Working...";

                var edatsInfo = edatInfo.GetInformation(pathTextBox.Text, recurseCheckBox.Checked);

                textBox.SelectionStart = textBox.Text.Length;
                Font curFont = textBox.SelectionFont;
                for (int i = 0; i < edatsInfo.Length; i++)
                {
                    textBox.AppendText(edatsInfo[i].filename + " ");

                    textBox.SelectionFont = new Font(curFont.FontFamily, curFont.Size, FontStyle.Bold);
                    textBox.AppendText(edatsInfo[i].cid + "\n");

                    textBox.SelectionFont = new Font(curFont.FontFamily, curFont.Size, FontStyle.Regular);
                    textBox.AppendText(String.Format("License: {0}. ", edatsInfo[i].license));

                    textBox.AppendText(String.Format("Version: {0}. ", edatsInfo[i].version));

                    textBox.AppendText(String.Format("Compression: {0}. ", edatsInfo[i].compression));

                    textBox.AppendText(String.Format("Digest: {0}\n", edatsInfo[i].digest));
                }
                textBox.ScrollToCaret();

                statusLabel.ForeColor = Color.Black;
                statusLabel.Text = "Idle...";
            }
        }
    }
}
